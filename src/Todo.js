import React from "react";
import "./custom.css";

class Todo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [], //one for display
      alllist: [], //one is for store real value
    };
  }

  //add function - add value in list with click and enter event
  add() {
    var input = document.getElementById("inputvalue").value;
    if (input !== "") {
      const userinput = {
        id: this.state.list.length,
        value: input,
        done: false,
      };
      const list = [...this.state.alllist];
      list.push(userinput);

      this.setState({
        list: list,
        alllist: list,
      });
      document.getElementById("inputvalue").value = "";
      console.log(this.state.list);
    }
  }

  //done function - task is done then mark as done=true
  done(id) {
    const list = this.state.list;
    let newlist = list.map((task) => {
      return task.id === id ? { ...task, done: !task.done } : { ...task };
    });
    this.setState({ list: newlist, alllist: newlist });
  }

  //delete function - delete perticular item form list
  delete(id) {
    const list = this.state.list;
    let newlist = list.filter((task) => {
      return task.id !== id;
    });
    this.setState({ list: newlist, alllist: newlist });
  }

  //dropdown list for filter view of list
  allfilter(e) {
    var selectval = e.target.value;
    console.log(selectval);
    const mlist = this.state.alllist;
    if (selectval === "all") {
      this.setState({ list: mlist });
      //we can do this with refs also
      document.getElementById("inputvalue").style.visibility = "visible";
      document.getElementById("inputbtn").style.visibility = "visible";
      document.getElementById("cleanbtn").style.visibility = "visible";
    } else if (selectval === "completed") {
      let clist = mlist.filter((task) => {
        return task.done === true;
      });
      this.setState({ list: clist });
      document.getElementById("inputvalue").style.visibility = "hidden";
      document.getElementById("inputbtn").style.visibility = "hidden";
      document.getElementById("cleanbtn").style.visibility = "hidden";
    } else if (selectval === "uncomplete") {
      let unclist = mlist.filter((task) => {
        return task.done === false;
      });
      this.setState({ list: unclist });
      document.getElementById("inputvalue").style.visibility = "hidden";
      document.getElementById("inputbtn").style.visibility = "hidden";
      document.getElementById("cleanbtn").style.visibility = "hidden";
    }
  }

  //clear function - clear all completed task
  clear() {
    const mlist = this.state.list;
    let clist = mlist.filter((task) => {
      return task.done === false;
    });
    this.setState({ list: clist, alllist: clist });
  }

  //rander
  render() {
    return (
      <>
        <h2>Todo List</h2>
        <hr />
        <input
          type="text"
          id="inputvalue"
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              this.add();
            }
          }}
        ></input>
        <input
          type="button"
          id="inputbtn"
          className="btn"
          onClick={() => this.add()}
          value="ADD"
        ></input>
        <select
          name="DisplayOption"
          id="dispalyoption"
          onChange={(e) => this.allfilter(e)}
        >
          <option value="all">All</option>
          <option value="completed">Completed</option>
          <option value="uncomplete">Uncomplete</option>
        </select>
        <input
          type="button"
          id="cleanbtn"
          className="clrbtn"
          onClick={() => this.clear()}
          value="Clear Completed"
        ></input>
        {this.state.list.map((list) => {
          return (
            <div className={list.done ? "line list" : "list"} key={list.id}>
              {list.value}
              <input
                type="button"
                value="Done"
                className="minibtn1"
                onClick={() => this.done(list.id)}
              />
              <input
                type="button"
                value="Delete"
                className="minibtn2"
                onClick={() => this.delete(list.id)}
              />
            </div>
          );
        })}
      </>
    );
  }
}

export default Todo;
